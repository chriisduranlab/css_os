<div align="center">
  
  [<img src="https://i.ibb.co/8bV56kt/preview.png">](https://www.instagran.com/chriis.duran)
  
</div>


- - -

###### CSS_OS 

* Use CSS to design a mobile UI with Menu Bottom Navigation. 

<div align="center">
  
  [<img src="https://i.ibb.co/WHXfGWk/previewresult.png" width="455px">](https://vimeo.com/848467696?share=copy)
  
</div>

- - -
##### Run it!
    Just you need open the index.html or use Live Server.
- - -
##### Software and technologies used

<p align="center">
  

  [<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/2048px-HTML5_logo_and_wordmark.svg.png" width="55px">](https://lenguajehtml.com/html/)
  [<img src="https://cdn-icons-png.flaticon.com/512/5968/5968242.png" width="55px">](https://lenguajecss.com/css/introduccion/que-es-css/)
  [<img src="https://cdn.iconscout.com/icon/free/png-256/free-jquery-8-1175153.png" width="55px">](https://jquery.com/)
  [<img src="https://pbs.twimg.com/profile_images/1491038861224517637/s-H1KgWO_400x400.png" width="55px">](https://fontawesome.com/)

</p>

- - -
##### Developers

<div align="left">
  
  [<img src="https://i.ibb.co/vX2mShm/chrisduran.png" width="55px">](https://linktr.ee/chriisduran)
  
</div>
